<?php get_template_part('parts/header'); the_post(); ?>

<main>

<?php 
  //slider repeater field group
  
  if (have_rows('showcase_slider') ) :
?>

<section class="slider">
  <div class="slider__track--showcase is-slider">

    <?php 
      while (have_rows('showcase_slider') ) : the_row(); 

        $bg = get_sub_field('showcase_slides')
    ?>

     <li class="slider__item" style="background-image: url('<?php echo $bg; ?>');">
        <div class="overlay clearfix">
      
        </div>
     </li>

    <?php endwhile; ?>

  </div>

  <?php 
    $text = get_field('showcase_text');
    $cs = get_field('showcase_customer');
    $year = get_field('showcase_year');
    $size = get_field('showcase_size');
    $addons = get_field('showcase_addons');
    $price = get_field('showcase_price');
  ?>

  <div class="slider__caption slider__caption--showcase">
    <div class="wrap hpad slider__container slider__container--showcase">
      
      <?php if (in_category('5') ) : ?>
      <div class="fourcol onecol-offset slider__factbox slider__factbox--gray-dark">
      
      <?php elseif (in_category('4') ) : ?>
      <div class="fourcol onecol-offset slider__factbox slider__factbox--green-dark">
      
      <?php elseif (in_category('6') ) : ?>
      <div class="fourcol onecol-offset slider__factbox slider__factbox--green">
      <?php endif; ?>
        <div class="toggle-acc active"><?php _e('Luk info', 'lionlab') ?></div>
        <div class="accordion">
          <h2 class="slider__factbox--title title-hr title-hr--white"><?php the_title(); ?></h2>
          <p><?php echo $text; ?></p><br>
          <?php if ($cs) : ?>
          <strong>Kunde:</strong>         <span><?php echo $cs; ?></span><br>
          <?php endif; ?>
          <?php if ($year) : ?>
          <strong>Færdigt:</strong>       <span><?php echo $year; ?></span><br>
          <?php endif; ?>
          <?php if ($size) : ?>
          <strong>Størrelse:</strong>     <span><?php echo $size; ?></span><br>
          <?php endif; ?>
          <?php if ($addons) : ?>
          <strong>Væsentligste tilvalg:</strong> <span><?php echo $addons; ?></span><br>
          <?php endif; ?>
          <?php if ($price) : ?>
          <strong>Pris:</strong>          <span><?php echo $price; ?></span><br>
          <?php endif; ?>
        </div>
      </div>

    </div>
  </div>
</section>

<?php endif; ?>

  <section class="wrap clearfix padding--both wow fadeInUp">

    <div class="hpad clearfix"><h1 class="page__title twelvecol"><?php the_title(); ?></h1></div>

    <?php $bp = get_field('showcase_blueprint'); ?>

    <section>
      <div class="wrap hpad clearfix showcase__container">
        <?php if ($bp) : ?>
        <div class="sixcol showcase__blueprint">

          <a class="js-zoom gallery__link" data-type="image" data-fancybox="images" href="<?php echo $bp; ?>">
            <img src="<?php echo $bp; ?>">
          </a>

          <ul class="showcase__list">
            <li class="showcase__list--item green-dark">Opholdsrum</li>
            <li class="showcase__list--item green">Køkken/bad</li>
            <li class="showcase__list--item grey-dark">Soverum</li>
            <li class="showcase__list--item grey">Mellemgang</li>
          </ul>

        </div>
        <?php endif; ?>

        <?php 
        //showcase addons chosen repeater field group 
        if (have_rows('showcase_box') ) : ?>

        <div class="fivecol onecol-offset gray--bg showcase__addons">

          <h3 class="title-hr title-hr--white">Væsentligste tilvalg</h3>

          <ul>
          <?php 
             while (have_rows('showcase_box') ) : the_row(); 

                $addons_chosen = get_sub_field('showcase_addons_chosen');
                $index   = get_row_index();
          ?>
          
          
            <li><strong><?php echo $index; ?>.</strong> <?php echo $addons_chosen; ?></li>

          <?php endwhile;  ?>
          </ul>

        </div>
        <?php endif; ?>

      </div>
    </section>


  </section>


   <section class="padding--both fadeInUp wow">
    <div class="wrap hpad clearfix">

        <?php 
          $image = get_field('testimonial_img');
          $name = get_field('testimonial_name');
          $text = get_field('testimonial_text');
          $video = get_field('testimonial_video');
       ?>

      <?php 
        if (have_rows('intro_img') ) :
      ?>

      <?php if ($name && $text) : ?>
     
      <div class="fivecol showcase__sketches center">
      <?php else: ?>
      <div class="tvelwecol showcase__sketches center"> 
      <?php endif; ?>

        <?php while (have_rows('intro_img') ) : the_row(); 
          $img = get_sub_field('img');

          $i = get_row_index();
        ?>

        <img class="showcase__img showcase__img--<?php echo $i; ?>" src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
          

        <?php endwhile; ?>
      </div>

      <?php endif; ?>
      
      <?php if ($name && $text) : ?>
      <div class="fivecol twocol-offset testimonial__item testimonial__item--single border--black">
        <?php if ($image) : ?>
        <img class="testimonial__img testimonial__img--single" src="<?php echo $image ?>">
        <?php endif; ?>
        <h2 class="title-hr title-hr--black testimonial__title"><?php echo $name; ?></h2>
        <em class="testimonial__quote"><?php echo $text; ?></em>
        
        <?php if ($video) : ?>
          <a data-fancybox="video" data-fancybox-type="iframe" class="btn btn--black" href="<?php echo $video ?>">Se video <i class="fas fa-angle-double-right"></i></a>
        <?php endif; ?>
      </div>
      <?php endif; ?>
      
    </div>
   </section>

   <?php get_template_part('parts/gallery'); ?>

   <?php get_template_part('parts/contact-form'); ?>

</main>

<?php get_template_part('parts/footer'); ?>