jQuery(document).ready(function($) {

  var owl = $(".slider__track");

  owl.owlCarousel({
      loop: true,
      nav: false,
      items: 1,
      autoplay: true,
      dots: false,
      autoplay: 15000,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<ion-icon name='arrow-back'></ion-icon>","<ion-icon name='arrow-forward'></ion-icon>"]
  });


  var owl = $(".slider__track--showcase");

  owl.owlCarousel({
      loop: true,
      nav: true,
      items: 1,
      autoplay: true,
      dots: false,
      autoplay: 15000,
      autplaySpeed: 11000,
      autoplayTimeout: 10000,
      smartSpeed: 250,
      smartSpeed: 2200,
      navSpeed: 2200,
      navText : ["<i class='ion-ios-arrow-left'></i>","<i class='ion-ios-arrow-right'></i>"]
  });


  //fire animation on load
  window.onload = function() {
    document.body.classList.add('fire');
  }

  //active menu link
  $(".current-menu-item a").each(function() {   
      if (this.href == window.location.href) {
          $(this).addClass("is-active");
      }
  });

  //menu toggle
  $('.nav-toggle').click(function(e) {
    e.preventDefault();
    $('.nav--mobile').toggleClass('is-open');
    $('body').toggleClass('is-fixed');
    $('.close-menu').toggleClass('is-fixed');
    $('.main-nav').delay(300).slideToggle(600);

    $(".nav-toggle__label").fadeOut(function () {
      $(".nav-toggle__label").text(($(".nav-toggle__label").text() == 'menu') ? 'Luk' : 'menu').fadeIn();
    })
  });

  $('.close-menu').click(function(e) {
    e.preventDefault();
    $('.close-menu').toggleClass('is-closed');
    $('.close-menu').removeClass('is-fixed');
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.main-nav').slideUp();

    $(".nav-toggle__label").fadeOut(function () {
      $(".nav-toggle__label").text(($(".nav-toggle__label").text() == 'menu') ? 'Luk' : 'menu').fadeIn();
    })
  });


  if($('body').is('.home') && !$('body').is('.is-safari')){
   //page transition
   $(".animsition").animsition({
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 1500,
      outDuration: 800,
      linkElement: '.animsition-link',
      // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
      loading: true,
      loadingParentElement: 'html', //animsition wrapper element
      loadingClass: 'animsition-loading',
      loadingInner: '<img src="wp-content/themes/moenhuset/assets/img/loading_bar_moenhuset.gif">', // e.g '<img src="loading.svg" />' 
      timeout: true,
      timeoutCountdown: 3004,
      onLoadEvent: false,
      browser: [ 'animation-duration', '-webkit-animation-duration'],
      // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
      // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
      overlay : false,
      overlayClass : 'animsition-overlay-slide',
      overlayParentElement : 'body',
      transition: function(url){ window.location.href = url; }
    });
  }

  if($('body').is('.home') && $('body').is('.is-safari')) {
   //page transition
   $(".animsition").animsition({
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 1500,
      outDuration: 800,
      linkElement: '.animsition-link',
      // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
      loading: true,
      loadingParentElement: 'html', //animsition wrapper element
      loadingClass: 'animsition-loading',
      loadingInner: '<img src="wp-content/themes/moenhuset/assets/img/logo.png">', // e.g '<img src="loading.svg" />' 
      timeout: true,
      timeoutCountdown: 3004,
      onLoadEvent: false,
      browser: [ 'animation-duration', '-webkit-animation-duration'],
      // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
      // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
      overlay : false,
      overlayClass : 'animsition-overlay-slide',
      overlayParentElement : 'body',
      transition: function(url){ window.location.href = url; }
    });
  }

//page transition
   $(".animsition").animsition({
      inClass: 'fade-in',
      outClass: 'fade-out',
      inDuration: 1500,
      outDuration: 800,
      linkElement: '.animsition-link',
      // e.g. linkElement: 'a:not([target="_blank"]):not([href^="#"])'
      loading: false,
      loadingParentElement: 'html', //animsition wrapper element
      loadingClass: 'animsition-loading',
      loadingInner: '', // e.g '<img src="loading.svg" />' 
      timeout: true,
      timeoutCountdown: 1004,
      onLoadEvent: false,
      browser: [ 'animation-duration', '-webkit-animation-duration'],
      // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
      // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
      overlay : false,
      overlayClass : 'animsition-overlay-slide',
      overlayParentElement : 'body',
      transition: function(url){ window.location.href = url; }
    });

  //mixit up
  if($('body').is('.blog, .home')){

    // Instantiate and configure the mixer
    var mixer = mixitup('.mixit', {

    });

  }


  //accordion toggle
  var $acc = $(".toggle-acc");
  $acc.each(function() {
      $(this).click(function() {
        $('.accordion').slideToggle("slow", function() {
          $(this).toggleClass("show");
        });

        $('.toggle-acc').each(function () {
          $('.toggle-acc').text(($('.toggle-acc').text() == 'Vis info') ? 'Luk info' : 'Vis info').fadeIn(); 
        })
      });
  });

  var $acc = $(".toggle-acc--popup, .icon--popup");
  $acc.each(function() {
      $(this).click(function() {
        // $('.accordion--popup').slideToggle("slow", function() {
          $('.accordion--popup').toggleClass("show");
        // });

        
        $('.toggle-acc--popup').each(function () {
          $(this).text(($(this).text() == 'Luk') ? 'Spørgsmål?' : 'Luk').fadeIn(); 
        })

      });
  });


  //accordion toggle active class
  $('.toggle-acc, .toggle-acc--popup, .cta__text--popup').click(function() {
    $(this).toggleClass('active'); 
  });


  //fancybox
  $('[data-fancybox="images"]').fancybox({
    toolbar: true,
    loop: false,
    buttons: [
        "zoom",
        "share",
        "slideShow",
        "fullScreen",
        "download",
        "thumbs",
        "close"
    ],
    thumbs : {
      autoStart : true,
      axis      : 'x'
    }
  });

  //No hashtag - fancybox closes at once
  $.fancybox.defaults.hash = false;


  //Video - Fancybox
  $('[data-fancybox="video"]').fancybox({
    type: 'iframe',
    'width': 800,
    'height': 550
  });


  //reveal animation
  wow = new WOW(
    {
    boxClass:     'wow',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       true,       // default
    live:         true        // default
  })

  wow.init();



  //Close UX modal
  $('.btn--close').click(function(e) {
    e.preventDefault();
    $('.mobile-detection').fadeOut('is-closed'); 
  });


  /**
   * Instagram feed
   **/ 

  // var $instagram = $('#instagram-feed');

  // if ($instagram.length ) {
  //   var username = $instagram.data('username');
  //   var feed = new Instafeed({
  //     clientId: '3b7ed3c55988457188e8ecae7eed2864',
  //     accessToken: '3700271252.3b7ed3c.39faf359e4194b988a1dbd83669af7f0', 
  //     get: 'user',
  //     userId: 3700271252,
  //     target: 'instagram-feed',
  //     resolution: 'standard_resolution',
  //     limit: 8,
  //     mock: true,
  //     success: function(images) {
  //       $.each(images.data, function(imageID, post) {
  //         var highResThumb = post.images.thumbnail.url.replace('s150x150', 's150x150');
  //         $instagram.append('<a class="instagram__item" target="_blank" href="' + post.link + '"><img class="instagram__image" src="' + highResThumb +'" alt="' + post.caption.text + '"></a>');
  //       });
  //     }
  //   });

  //   feed.run();
  // }

});
