<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header'); ?>

  <section class="wrap hpad clearfix padding--bottom">


    <?php
      $category_name = "huse";
      $category = get_category_by_slug( $category_name );
      $category_id = $category->term_id;

      $args = array('child_of' => $category_id);
      $categories = get_categories( $args );
   ?>

   <div class="home__controls mixit_controls flex flex--wrap onecol-offset">
        <div class="home__filter" data-filter="all"><span>Alle</span></div>
      <?php foreach($categories as $category) : ?>
        <div class="home__filter" data-filter=".cat<?php echo $category->term_id;?>"><span><?php echo $category->name; ?></span></div> 
      <?php endforeach; ?>
  </div>

  <div class="mixit home__row">

    <?php if (have_posts()): ?>
      <?php while (have_posts()): the_post(); ?>

      <?php 
          $cats = get_the_category();
          $cat_string = "";

          foreach ($cats as $cat) {
            $cat_string .= " cat" . $cat->term_id ."";
          }
      ?>

      <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );?>

      <?php 
        $excerpt = get_field('home_excerpt');
        $overlay = get_field('home_overlay');

        //color class

        if ($overlay === 'green') {
          $class = 'overlay overlay--green--bg';
        }

        elseif ($overlay === 'green-dark') {
          $class = 'overlay overlay--green-dark--bg';
        }

        elseif ($overlay === 'gray') {
          $class = '';
        }

        elseif ($overlay === 'gray-dark') {
          $class = 'overlay overlay--gray-dark--bg';
        }
      ?>

      <a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" class="<?php echo $class; ?> mix <?php echo $cat_string; ?> home__post fourcol" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url('<?php echo $thumb['0'];?>')">
          
        <div class="home__content-wrap">
          <header>
            <h2 class="home__post--title title-hr title-hr--white">
                <?php the_title(); ?>
            </h2>
          </header>

          <div class="home__post--excerpt">
            <?php echo $excerpt; ?>
          </div>

          <strong class="btn--nb home__post--btn">Se projekt »</strong>
        </div>

      </a>

      <?php endwhile; else: ?>

        <p>No posts here.</p>

    <?php endif; ?>

  </div>

  </section>

  <?php get_template_part('parts/process'); ?>

  <?php get_template_part('parts/contact-form'); ?>

</main>

<?php get_template_part('parts/footer'); ?>