<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'hero_unit' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'hero_unit' ); ?>

    <?php
    } elseif( get_row_layout() === 'flexible_columns' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'flexible_columns' ); ?>

    <?php
    } elseif( get_row_layout() === 'staggered_images_with_text' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'staggered_images_with_text' ); ?>

    <?php
    } elseif( get_row_layout() === 'slider' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'slider' ); ?>

    <?php
    } elseif( get_row_layout() === 'employees' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'intro' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'intro' ); ?>

    <?php
    } elseif( get_row_layout() === 'concept' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'concept' ); ?>

    <?php
    } elseif( get_row_layout() === 'modules' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'modules' ); ?>

    <?php
    } elseif( get_row_layout() === 'materials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'materials' ); ?>

    <?php
    } elseif( get_row_layout() === 'testimonials' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'testimonials' ); ?>

    <?php
    } elseif( get_row_layout() === 'process' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'process' ); ?>

    <?php
    } elseif( get_row_layout() === 'about' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'about' ); ?>

    <?php
    } elseif( get_row_layout() === 'about-mosaic' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'about-mosaic' ); ?>

    <?php
    } elseif( get_row_layout() === 'video' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'video' ); ?>

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>

<?php
}
?>
