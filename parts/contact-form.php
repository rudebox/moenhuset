<?php 
	//contact form field group from options page

	$title = get_field('contact_title', 'options');
	$text = get_field('contact_text', 'options');
 ?>

 <?php if (is_front_page() ) : ?>
 <section class="contact contact--front padding--both">
 <?php else: ?>
 <section class="contact padding--both">
 <?php endif; ?>
 	<div class="wrap hpad contact__container">
 		<h2 class="center title-hr title-hr--center contact__title"><?php echo $title; ?></h2>
 		<div class="center sixcol threecol-offset contact__intro"><?php echo $text; ?></div>

 		<?php gravity_form( 2, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
 	</div>
 </section>