<?php
//counter
$i=0;

//gallery
$gallery = get_field('gallery');
$index   = get_row_index();

if ( $gallery ) : ?>

	<section class="gallery padding--bottom fadeInUp wow">
		<div class="wrap hpad">
			<h2>Galleri</h2>

			<div class="row gallery__list flex flex--wrap">

				<?php
				// Loop through gallery
				foreach ( $gallery as $image ) : $i++; 

						if ($i >= 7) :

							$hide = 'hidden';

						else:

							$hide = '';

						endif;
					?>

					<a href="<?php echo $image['url']; ?>" class="gallery__item gallery__link js-zoom gallery__item--<?php echo $i; ?> <?php echo $hide; ?>" style="background-image: url(<?php echo $image['url']; ?>)" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" data-type="image" data-fancybox="images" data-height="<?= $image['sizes']['medium']; ?>" data-width="<?= $image['sizes']['medium']; ?>">
					</a>

				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>