<?php 
	if (is_home() ) {
		$page_title = get_the_title(get_option('page_for_posts'));
	}

	elseif ( is_category() ) {
		$page_title = single_cat_title( '', false );
	}

	elseif ( is_archive() ) {
		$page_title = post_type_archive_title( '', false );
	}

	elseif ( is_404() ) {
        $title = __( 'Siden findes ikke' );
    }

	else {
		$page_title = single_post_title( '', false );  
	}

	$custom_page_title = ( get_field('page_titel') ?: get_field('page_titel', 'options') );

?>

<?php if ($custom_page_title) : ?>
<section class="page__header page__header--blank">
	<div class="wrap hpad clearfix">
		<h1 class="page__title onecol-offset sixcol"><?php echo $custom_page_title; ?></h1>
	</div>
</section>

<?php else : ?>

<section class="page__header">
	<div class="wrap hpad clearfix">
		<h1 class="page__title onecol-offset sixcol"><?php echo $page_title; ?></h1>
	</div>
</section>

<?php endif; ?>