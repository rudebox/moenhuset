<?php 
	//counter
	$i=0;


	//field groups
	$heading = get_sub_field('testimonial_header');
	$bg = get_sub_field('testimonial_bg');

	//testimonials repeater field group
	if (have_rows('testimonial_box') ) :
 ?>

 <section class="testimonial gray-dark--bg" style="background-image: url(<?php echo $bg; ?>);">
 	<div class="wrap hpad clearfix testimonial__container">
 		<h2 class="testimonial__heading"><?php echo $heading; ?></h2>

 		<div class="row flex flex--wrap flex--center">
 			<?php 
 				while (have_rows('testimonial_box') ) : the_row();
 					$img = get_sub_field('testimonial_img');
 					$name = get_sub_field('testimonial_name');
 					$text = get_sub_field('testimonial_text');
 					$video = get_sub_field('testimonial_video');

 					$i++;

 					if ($i === 2) {
 						echo '<div class="fivecol twocol-offset flex testimonial__wrapper">';
 					}
 			?>

 			<div class="fourcol onecol-offset testimonial__item border--white">
 				<img class="testimonial__img" src="<?php echo $img['sizes']['testimonial']; ?>" alt="<?php echo $img['alt']; ?>">
 				<h2 class="title-hr title-hr--white testimonial__title"><?php echo $name; ?></h2>
 				<em class="testimonial__quote"><?php echo $text; ?></em>
				
				<?php if ($video) : ?>
					<a data-fancybox="video" data-fancybox-type="iframe" class="btn btn--white" href="<?php echo $video; ?>">Se video <i class="
						icon">»</i></a>
				<?php endif; ?>
 			</div>

 			<?php 
	 			if ($i === 3) {
 					echo '</div>';
 				}
 			 ?>

 			<?php endwhile; ?>
 		</div>
 	</div>
 </section>
 <?php endif; ?>