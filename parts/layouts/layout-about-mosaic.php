<?php 
	
	//layout type: about mosaic field group

	$company = get_sub_field('company_text');
	$vision = get_sub_field('vision_text');
	$img_sm = get_sub_field('img_sm');
	$factbox = get_sub_field('factbox_text');
	$img_l = get_sub_field('img_l');
	$mission = get_sub_field('mission_text');
	$history = get_sub_field('history_text');
 ?>

 <section class="mosaic padding--bottom fadeInUp wow">
 	<div class="wrap hpad clearfix mosaic__container">
 		<div class="row--flex flex--wrap">
 			<div class="fivecol onecol-offset gray-dark--bg mosaic__whitespace mosaic__box">
 				<?php echo $company; ?>
 			</div>
 			<div class="fivecol border--black mosaic__box--middle mosaic__whitespace">
 				<?php echo $vision; ?>
 			</div>

 			<div class="fourcol mosaic__img mosaic__whitespace" style="background-image: url(<?php echo $img_sm; ?>);">
 				
 			</div>
 			<div class="twocol green--bg mosaic__box--small mosaic__whitespace">
 				<?php echo $factbox; ?>
 			</div>
 			<div class="sixcol mosaic__img mosaic__img--offset mosaic__whitespace" style="background-image: url(<?php echo $img_l; ?>);">
 				
 			</div>

 			<div class="fourcol onecol-offset border--black mosaic__box--start mosaic__box center mosaic__whitespace">
 				<?php echo $mission; ?>
 			</div>

 			<div class="fivecol green-dark--bg mosaic__box mosaic__box--large mosaic__whitespace">
 				<div class="mosaic__wrap onecol-offset">
 					<?php echo $history; ?>
 				</div>
 			</div>	

 		</div>
 	</div>
 </section>