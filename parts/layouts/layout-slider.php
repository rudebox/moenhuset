<?php 
  //slider repeater field group
  $type = get_sub_field('type');
?>


<?php if ($type === 'slider') : ?>
<section class="slider">
  <div class="slider__track is-slider">
    <?php if(get_sub_field('slider_slides')) { ?>

      <?php while(has_sub_field('slider_slides')) { ?>
        <li class="slider__item"
            style="background-image: url('<?php the_sub_field('slider_bg'); ?>');">
          <div class="overlay clearfix">
      
          </div>
        </li>
      <?php } ?>
      
    <?php } ?>

  </div>

  <?php 
    $text = get_sub_field('slider_text');
   ?>

  
  <div class="slider__caption">
    <div class="wrap hpad slider__container">
       <div class="slider__text threecol">
         <p class="slider__title"><?php echo $text; ?></p>
       </div>
    </div>
  </div>

</section>
<?php endif; ?>

<?php if ($type === 'video') : ?>
<?php 
// Hero section

$poster = get_sub_field('video_poster');
?>

<section class="video__wrap">
  <div class="video" style="background-image: url(<?php echo $poster['url']; ?>);">
    
    <?php 
    /**
     * Video
     **/ 
    $video_mp4 = get_sub_field('video_mp4');
    $video_ogv = get_sub_field('video_ogv');
    $video_webm = get_sub_field('video_webm');
    ?>
    
    <video id="video__hero" class="video__hero" autoplay preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
      <source src="<?php echo $video_mp4; ?>" type="video/mp4" codecs="avc1, mp4a">
      <source src="<?php echo $video_ogv; ?>" type="video/ogg" codecs="theora, vorbis">
      <source src="<?php echo $video_webm; ?>" type="video/webm" codecs="vp8, vorbis">
      Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
    </video>

    <?php $video_text = get_sub_field('slider_text'); ?>
    
  <div class="slider__caption">
    <div class="wrap hpad slider__container">
       <div class="slider__text threecol">
         <p class="slider__title"><?php echo $video_text; ?></p>
       </div>
    </div>
  </div>

  </div>
</section>
<?php endif; ?>