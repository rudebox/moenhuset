<?php 
	//about field group
	
	$title = get_sub_field('about_title');
	$text = get_sub_field('about_text');
	$link = get_sub_field('about_link');
	$img = get_sub_field('about_img');
	$imgbw = get_sub_field('about_imgbw');
	$fcbox = get_sub_field('about_factbox');
 ?>

 <section class="about padding--both">
 	<div class="wrap hpad clearfix about__container">
 		<div class="row clearfix">
 			<div class="fivecol onecol-offset about__box border--black first wow fadeInLeft">
 				<h2 class="title-hr about__title"><?php echo $title; ?></h2>
 				<p><?php echo $text; ?></p>
 				<a class="btn btn--black" href="<?php echo $link; ?>">Se mere <i class="icon">»</i></a>
 			</div>

 			<div class="sixcol about__img first wow fadeInUp">
 				<img src="<?php echo $img; ?>"> 				
 			</div>
 		</div>

 		<div class="row--flex flex--wrap about__row">
 			<div class="sevencol fivecol-offset first about__wrap wow fadeInRight">
 				<div class="eightcol about__img--bg" style="background-image: url(<?php echo $imgbw; ?>);">
 					
 				</div>
 				<div class="threecol halfcol-offset green--bg about__factbox">
 					<?php echo $fcbox; ?>
 				</div>
 			</div>
 		</div>
 	</div>
 </section>