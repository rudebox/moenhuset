<?php global $layout_count; 

  $cs_class = get_sub_field('use_custom_class');
  
  //color class
  $bg = get_sub_field('bg');


  if ($bg === 'green') {
    $class = 'green--bg';
  }

  elseif ($bg === 'green-dark') {
    $class = 'green-dark--bg';
  }

  elseif ($bg === 'gray-dark') {
    $class = 'gray-dark--bg';
  }

  elseif ($bg === 'gray') {
    $class = 'gray--bg';
  }
?>
<section id="section-<?php echo $layout_count; ?>" class="section--columns <?php echo $class; ?> <?php echo $cs_class; ?>">
  <div class="wrap hpad clearfix">
    <?php if(get_sub_field('header')): ?>
      <h2 class="title-hr"><?php the_sub_field('header'); ?></h2>
    <?php endif; ?>
    <?php
    if(get_sub_field('offset') !== 'Flexible') {
        $flex = false;
        if(get_sub_field('offset') === '2 to 1') {
          $offset = '2:1';
        } else {
          $offset = '1:2';
        }
      } else {
        $flex = true;
        $offset = null;
      }
      scratch_layout_declare(get_sub_field('wysiwygs'), 2, $flex, $offset);
      while(has_sub_field('wysiwygs')) {
        scratch_layout_start();
          the_sub_field('wysiwyg');
        scratch_layout_end();
      }
    ?>
  </div>
</section>
