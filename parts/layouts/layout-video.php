<?php 
// Hero section

$poster = get_sub_field('video_poster');
?>

<section class="video__wrap margin--both">
	<div class="video" style="background-image: url(<?php echo $poster['url']; ?>);">
		
		<?php 
		/**
		 * Video
		 **/ 
		$video_mp4 = get_sub_field('video_mp4');
		$video_ogv = get_sub_field('video_ogv');
		$video_webm = get_sub_field('video_webm');
		?>
		
		<video id="video__hero" class="video__hero" autoplay preload="auto" loop muted="muted" volume="0" poster="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVQ">
			<source src="<?php echo $video_mp4; ?>#t=6" type="video/mp4" codecs="avc1, mp4a">
			<source src="<?php echo $video_ogv; ?>#t=6" type="video/ogg" codecs="theora, vorbis">
			<source src="<?php echo $video_webm; ?>#t=6" type="video/webm" codecs="vp8, vorbis">
			Din browser understøtter ikke HTML5 video. Opgrader venligst din browser.
		</video>

		<?php $video_text = get_sub_field('video_text'); ?>
		
		<div class="video__inner wrap hpad clearfix">
			<div class="tencol onecol-offset">
				<p class="border--white video__text"><?php echo $video_text; ?></p>
			</div>
		</div> 
	</div>
</section>