<?php 
	//intro section field group

	$box = get_sub_field('intro_box');
	$quote = get_sub_field('intro_quote');
	$quote_by = get_sub_field('intro_quoteby');
 ?>

 <section class="intro padding--bottom">
 	<div class="wrap hpad clearfix intro__container">
 		<div class="row">

 			<?php 
				
				if (have_rows('intro_img') ) :
				
			?>

			<div class="fivecol intro__col intro__col--img">
				<?php while (have_rows('intro_img') ) : the_row(); 
 					$img = get_sub_field('img');

 					$i = get_row_index();
 				?>

 				<img class="intro__img intro__img--<?php echo $i; ?>" src="<?php echo $img['sizes']['intro']; ?>" alt="<?php echo $img['alt']; ?>">
 					

 				<?php endwhile; ?>
 			</div>

 			<?php endif; ?>

 			<div class="fivecol intro__col onecol-offset intro__col--offset">
 				<div class="intro__textbox gray-dark--bg">
 					<?php echo $box; ?>
 				</div>

 				<p class="intro__quote">
 					<em class="intro__quote--text"><?php echo $quote; ?></em>
					<br>
 					<em class="intro__quote--by"><?php echo $quote_by; ?></em>
 				</p>
 			</div>

 		</div>
 	</div>
 </section>