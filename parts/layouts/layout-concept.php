<?php 
	//concept field group

	$title = get_sub_field('concept_title');
	$text = get_sub_field('concept_text');
	$link = get_sub_field('concept_link');
	$img = get_sub_field('concept_blueprint');
 ?>

 <section class="concept padding--both wow fadeInUp fadeIn">
 	<div class="wrap hpad clearfix concept__container">
 		<div class="row flex flex--wrap flex--valign">
 			<div class="fivecol onecol-offset border--black concept__box">
 				<h2 class="concept__title title-hr"><?php echo $title; ?></h2>
 				<?php echo $text; ?>
 				<a class="btn btn--black" href="<?php echo $link; ?>">Se mere <i class="icon">»</i></a>
 			</div>
 			<div class="fivecol center concept__img">
 				<img src="<?php echo $img; ?>">
 			</div>
 		</div>
 	</div>
 </section>