<?php global $layout_count; 

  $cs_class = get_sub_field('use_custom_class');
    
  //color class
  $bg = get_sub_field('flexible_bg');


  if ($bg === 'green') {
    $class = 'green--bg';
  }

  elseif ($bg === 'green-dark') {
    $class = 'green-dark--bg';
  }

  elseif ($bg === 'gray-dark') {
    $class = 'gray-dark--bg';
  }

  elseif ($bg === 'gray') {
    $class = 'gray--bg';
  }
?>
<section id="section-<?php echo $layout_count; ?>"
         class="section--flexible <?php echo $class; ?> <?php echo $cs_class; ?>">
  <div class="wrap hpad clearfix center">
    <?php if( get_sub_field('header')): ?>
      <h2><?php the_sub_field('header'); ?></h2>
    <?php endif; ?>
    <?php the_sub_field('blurb'); ?>
    <?php
      $columns = get_sub_field('use_custom_columns') ? 'custom' : 4;
      $icon_or_image = get_sub_field('icon_or_image') === 'Icon' ? 'icon' : 'image';
      scratch_layout_declare(get_sub_field('columns'), $columns);
      while(has_sub_field('columns')):
        scratch_layout_start();
    ?>

        <?php if($icon_or_image === 'icon'): ?>
          <?php if(get_sub_field('icon')): ?>
            <div class="flexible__text">
              <?php the_sub_field('icon'); ?>
            </div>
          <?php endif; ?>
        <?php else: ?>
          <?php if(get_sub_field('image')): ?>
            <div class="flexible__img"
                 style="background-image: url('<?php the_sub_field('image'); ?>');">
            </div>
          <?php endif; ?>
        <?php endif; ?>

        <?php if(get_sub_field('header')): ?>
          <h5 class="flexible__title"><?php the_sub_field('header'); ?></h5>
        <?php endif; ?>

        <?php if($icon_or_image === 'image'): ?>
          <?php if(get_sub_field('icon')): ?>
            <div class="flexible__text">
              <?php the_sub_field('icon'); ?>
            </div>
          <?php endif; ?>
        <?php else: ?>
          <?php if(get_sub_field('image')): ?>
            <div class="flexible__img"
                 style="background-image: url('<?php the_sub_field('image'); ?>');">
            </div>
          <?php endif; ?>
        <?php endif; ?>

    <?php
        scratch_layout_end();
      endwhile;
    ?>
  </div>
</section>
