<?php 
	//materials field group

	$title = get_sub_field('materials_title');
	$text = get_sub_field('materials_text');
	$link = get_sub_field('materials_link');
	$bg = get_sub_field('materials_bg');

	$n_title = get_sub_field('nature_title');
	$n_text = get_sub_field('nature_text');
	$n_img = get_sub_field('nature_img');
 ?>


 <section class="materials padding--bottom" style="background-image: url(<?php echo $bg['url']; ?>);">
 	<div class="wrap hpad clearfix materials__container">
 		<div class="row flex flex--wrap">
 			<div class="fivecol onecol-offset materials__box black--bg first wow fadeInLeft">
 				<h2 class="materials__title title-hr title-hr--white"><?php echo $title; ?></h2>
 				<p><?php echo $text; ?></p>
 				<a class="btn btn--white" href="<?php echo $link; ?>">Se mere <i class="icon">»</i></a>
 			</div>

			<div class="materials__wrapper fivecol flex flex--wrap">
	 			<?php 
	 				if (have_rows('materials_images') ) : while(have_rows('materials_images') ) : the_row();
	 					$img = get_sub_field('materials_img'); 
	 					$index = get_row_index();
	 			?>
	 			<div class="materials__img wow fadeInRight first materials__img--<?php echo $index; ?>" style="background-image: url(<?php echo $img; ?>);">
	 				
	 			</div>
	 			<?php endwhile; endif; ?>
 			</div>
 		</div>
 	</div>
 </section>

 <section class="nature padding--bottom">
 	<div class="wrap clearfix hpad">
	 	<div class="row flex flex--wrap materials__row">
	 		<div class="nature__wrapper sixcol intro__col intro__col--large">
	 		<div class="intro__col--border"></div>
	 		<div class="intro__col--border--top"></div>
 			<?php 
				if (have_rows('nature_images') ) : while(have_rows('nature_images') ) : the_row();
					$n_img = get_sub_field('nature_img'); 
					$index = get_row_index();
 			?>
			
				<img class="intro__img intro__img--<?php echo $index; ?>" src="<?php echo $n_img; ?>">
			
			<?php endwhile; endif; ?>
			</div>

			<div class="fivecol onecol-offset materials__box materials__box--gray first">
				<h3 class="title-hr title-hr--gray"><?php echo $n_title; ?></h3>
				<?php echo $n_text; ?>
			</div>
	 	</div>
 	</div>
 </section>

