<section class="instagram padding--top">
	<div class="wrap clearfix instagram__container">
		<h2 class="title-hr title-hr--gray title-hr--center center instagram__title">#mønhuset</h2>
	</div>

	<!-- Instagram -->
	<script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script>
	<iframe src="//lightwidget.com/widgets/c8f6dffee2e750cd845c9808c27f037d.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>



</section>		

<?php 
	//counter
	$i=0;
 ?>
<footer id="footer" class="clearfix footer gray--bg padding--both">
	<div class="wrap hpad">
		<div class="row">
			<?php
			/**
			 * Footer widgets
			 **/

			if ( have_rows('columns', 'options') ) :
				while ( have_rows('columns', 'options') ) :
					the_row();
					$title   = get_sub_field('title');
					$content = get_sub_field('text');

					//counter
					$i++;

					if($i === 2) :
						$column_class = 'large fivecol';

					else: 
						$column_class = '';

					endif;
					?>

					<div class="threecol footer__item <?php echo $column_class; ?>">
						<h5 class="footer__item--title"><?= $title ?></h5>
						<div class="footer__item--content"><?= $content ?></div>
					</div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>

<img src="<?php echo get_template_directory_uri(); ?>/assets/img/loading_bar_moenhuset.gif" style="display: none;" alt="">

<?php 
	$detect = get_field('mobile_text', 'options');
 ?>

<?php 
	// Show once every day, or when not visited before
	if (!isset($_COOKIE['showOnlyOnce'])) :
?> 

	<div class="wrap hpad mobile-detection">
		<div class="mobile-detection__modal">
			<?php echo $detect; ?>
			<a href="#" class="btn btn--black btn--close">Fortsæt <i class="icon">»</i></a>
		</div>	
	</div>
<?php endif; ?>


<?php wp_footer(); ?>

</body>
</html>