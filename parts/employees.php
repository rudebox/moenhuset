<?php  
	//employees repeater field group

	$heading = get_field('employee_heading', 'options');

	if (have_rows('employees', 'options') ) :
 ?>

<?php if (is_front_page() ) : ?>
 <section class="employee employee--front">
 <?php else: ?>
 <section class="employee">
 <?php endif; ?>
 	<div class="wrap hpad clearfix employee__container">
 		<h2 class="employee__title"><?php echo $heading; ?></h2>
 		<div class="row--offset employee__row">
 			
 			<?php 
 				while (have_rows('employees', 'options') ) : the_row();

 					$img = get_sub_field('employee_img');
 					$name = get_sub_field('employees_name');
 					$title = get_sub_field('employee_title');
 					$position = get_sub_field('employee_position');
 					$phone = get_sub_field('employee_phone');
 					$mail = get_sub_field('employee_mail');
 					$bg = get_sub_field('employee_bg');

	 				if ($bg === 'green') {
	 					$class = 'green--bg';
					}

					elseif ($bg === 'green-dark') {
						$class = 'green-dark--bg';
					}

					elseif ($bg === 'gray-dark') {
						$class = 'gray-dark--bg';
					}

					elseif ($bg === 'gray') {
						$class = 'gray--bg';
					}
 			 ?>

 			 <div class="fourcol employee__item <?php echo $class; ?>" style="background-image: url(<?php echo $img; ?>);">

 			 	<div class="employee__content">

	 			 	<?php if ($name) : ?>
	 			 	<h5 class="employee__name"><?php echo $name; ?></h5>
	 			 	<?php endif; ?>

	 			 	<?php if ($title) : ?>
	 			 	<?php echo $title; ?><br>
	 			 	<?php endif; ?>

	 			 	<?php if ($position) : ?>
	 			 	<?php echo $position; ?><br>
	 			 	<?php endif; ?>

	 			 	<?php if ($mail) : ?>
	 			 		E: <a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a><br>
	 			 	<?php endif; ?>

	 			 	<?php if ($phone) : ?>
	 			 		M: <a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo $phone; ?></a>
	 			 	<?php endif; ?>

 			 	</div>
 			 </div>

 			<?php endwhile; ?>
 		</div>
 	</div>
 </section>

<?php endif; ?>