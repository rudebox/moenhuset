<?php 
// Set cookie to expire after 24 hours
$expire = time()+60*60*24;
setcookie('showOnlyOnce', 'showOnlyOnce', $expire);
?>

<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<?php 
// Show once every day, or when not visited before
if (!isset($_COOKIE['showOnlyOnce'])) :
?> 

<body <?php body_class('animsition front'); ?>>
<?php else: ?>
<body <?php body_class('animsition'); ?>>
<?php endif; ?>

<!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<header class="header" id="header">
  <div class="wrap hpad header__container flex flex--justify flex--center">

    <a class="header__logo"
       href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('menu', 'lionlab') ?></span>
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>


<?php 
  //Fixed CTA catalog
  $text = get_field('catalog_text', 'options');
  $link = get_field('catalog_link', 'options');

  //Fixed CTA popup
  $p_text = get_field('popup_text', 'options');
  $p_content = get_field('popup_content', 'options');
  $p_phone = get_field('popup_phone', 'options');
  $p_mail = get_field('popup_mail', 'options');
  $p_img = get_field('popup_img', 'options');
 ?>

<div class="cta cta--popup">
  <div class="cta__link cta__popup">
     <div class="cta__text cta__text--popup">
      <i class="icon icon--popup">+</i><div class="toggle-acc--popup"><?php _e("$p_text", 'lionlab') ?></div>
     </div>
       <div class="accordion--popup" style="background-image: url(<?php echo $p_img['url']; ?>)">
          <div class="accordion__wrap">
            <p><?php echo $p_content; ?></p>
            <strong><?php echo $p_phone; ?></strong>
          </div>
          <a href="mailto:<?php echo esc_html($p_mail); ?>"><?php echo esc_html($p_mail); ?></a>
       </div>
   </div>
</div>

<?php if (!is_page('Bestil katalog') ) : ?>
<div class="cta">
   <a class="cta__link" href="<?php echo $link; ?>">
     <span class="cta__text"><i class="icon">+</i> <span><?php echo $text; ?></span></span>
   </a>
</div>
<?php endif; ?>

<div class="close-menu"></div>