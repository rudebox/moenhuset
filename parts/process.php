<?php
	//counter 
	$i=0;

	$heading = get_field('process_heading', 'options');

	//Layout type: process repeater field group

	if (have_rows('process_box', 'options') ) :
 ?>


 <section class="process wow fadeInUp">
 	<div class="wrap hpad clearfix process__container">
 		<h2 class="process__heading"><?php echo $heading; ?></h2>
 		<div class="row flex flex--wrap process__row">

	 		<?php while (have_rows('process_box', 'options') ) : the_row(); 
	 				$title = get_sub_field('process_title');
	 				$text = get_sub_field('process_text');
	 				$bg = get_sub_field('process_bg');

	 				if ($bg === 'green') {
	 					$class = 'green--bg';
					}

					elseif ($bg === 'green-dark') {
						$class = 'green-dark--bg';
					}

					elseif ($bg === 'gray-dark') {
						$class = 'gray-dark--bg';
					}

					elseif ($bg === 'gray') {
						$class = 'gray--bg';
					}

					$i++;
	 		?>

	 		<div class="threecol process__item <?php echo $class; ?>">
	 			<h5 class="process__title"><?php echo $i; ?>. <?php echo $title; ?></h5>
	 			<?php echo $text; ?>
	 		</div>

	 		<?php endwhile; ?>
 		</div>
 	</div>
 </section>
 <?php endif; ?>