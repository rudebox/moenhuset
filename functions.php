<?php

/* Do not remove this line. */
require_once('includes/scratch.php');

// define( 'GOOGLE_MAPS_KEY', '' );


/*
 * scratch_meta() adds all meta information to the <head> element for us.
 */

function scratch_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="shortcut icon" href="<?php echo get_site_url(); ?>/favicon.ico" type="image/x-icon">
  

<?php }

add_action('wp_head', 'scratch_meta');

/* Theme CSS */

function theme_styles() {

  wp_register_style( 'scratch-main', get_template_directory_uri() . '/assets/css/master.css', false, filemtime(dirname(__FILE__) . '/assets/css/master.css') );
  wp_enqueue_style( 'scratch-main' );

  wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css', false, null );

  wp_enqueue_style( 'ionicons', 'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.0/css/ionicons.min.css', false, null );

  wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.13/css/all.css', false, null );

}

add_action( 'wp_enqueue_scripts', 'theme_styles' );



/* Theme JavaScript */

function theme_js() {

  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', false, false, false );

  wp_enqueue_script( 'instafeed', get_template_directory_uri() . '/assets/js/vendor/instafeed.js', false, false, false ); 

  wp_register_script( 'scratch-main-concat', get_template_directory_uri() . '/assets/js/concat/main.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/concat/main.js'), true );

  wp_register_script( 'scratch-main-min', get_template_directory_uri() . '/assets/js/compiled/main.min.js', array('jquery'), filemtime(dirname(__FILE__) . '/assets/js/compiled/main.min.js'), true );

  /* FOR DEVELOPMENT */
  // wp_enqueue_script( 'scratch-main-concat' );

  /* FOR PRODUCTION */
  wp_enqueue_script( 'scratch-main-min' );

}

add_action( 'wp_enqueue_scripts', 'theme_js' );

/* Enable ACF Options Pages */

if(function_exists('acf_add_options_page')) {

  acf_add_options_page();
  acf_add_options_sub_page('Header');
  acf_add_options_sub_page('Generelt indhold');
  acf_add_options_sub_page('Blog');
  acf_add_options_sub_page('Sidebar');
  acf_add_options_sub_page('Footer');

}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'scratch-main-nav' => __( 'Main Nav', 'scratch' )   // main nav in header
  )
);

function scratch_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'scratch' ), // nav name
    'menu_class' => 'main-nav', // adding custom nav class
    'theme_location' => 'scratch-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0    // fallback function
  ));
} /* end scratch main nav */


// Custom excerpt length

function custom_excerpt_length( $length ) {
  return 40;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



// Custom excerpt text

function custom_excerpt_more( $more ) {
  return '&hellip;';
}


/**
 * Google Maps API key for ACF
 * @since 2.0.0
 **/

// if ( defined( 'GOOGLE_MAPS_KEY' ) && GOOGLE_MAPS_KEY !== null ) {
//   function add_google_maps_key_to_acf( $api ) {
//       $api['key'] = GOOGLE_MAPS_KEY;
//       return $api;
//   }

//   add_filter('acf/fields/google_map/api', 'add_google_maps_key_to_acf');
// }

// wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_KEY, array('jquery'), null, true );



// Move Yoast to bottom
function yoasttobottom() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


//remove emoji, embed scripts and styling for performance optimization
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


//remove countrycode from phone
function get_formatted_phone($str) {
  
  // Remove +45
  $str = str_replace('+', '00', $str);

  // Only allow integers
  $str = preg_replace('/[^0-9]/s', '', $str);

  return $str;
}


// Image sizes
add_image_size('intro', 285, 285, true);
add_image_size('testimonial', 160, 160, true);


//add browser classes to body tag
function browser_classes($classes) {

  // Browser specific classes based on user agent globals
  global $is_gecko, $is_IE, $is_opera, $is_safari, $is_chrome, $is_edge, $is_iphone;

  if ($is_gecko)          $classes[] = 'is-gecko';
  elseif ($is_opera)      $classes[] = 'is-opera';
  elseif ($is_iphone)       $classes[] = 'is-iphone';
  elseif ($is_safari)     $classes[] = 'is-safari';
  elseif ($is_chrome)     $classes[] = 'is-chrome';
  elseif ($is_IE)         $classes[] = 'is-ie';
  elseif ($is_edge)       $classes[] = 'is-edge';
  else                $classes[] = 'is-unknown';

  if (!is_front_page())   $classes[] = 'is-not-home';
  
  return $classes;
}

add_filter('body_class', 'browser_classes');

function lionlab_yt_shortcode( $atts ) {

  $atts = shortcode_atts(
    array(
      'id' => '',
      'url' => '',
    ),
    $atts
  );


  return '<div class="embed embed__item embed--16-9"><a class="embed__link" style="background-image: url(https://img.youtube.com/vi/' . $atts['id'] . '/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="' . $atts['url'] . '?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;"><i class="mejs-overlay-button"></i></a>
    </div>';

  $output = ob_get_clean();
  echo $output;

}

add_shortcode( 'lionlab_yt', 'lionlab_yt_shortcode' );
/* Place custom functions below here. */

  //accordion shortcode
  function fold_shortcode( $atts , $content = null ) {

      $atts = shortcode_atts(
        array(
          'titel' => '',
        ),
        $atts
      );

    return '<div class="accordion__wrapper"><h3 class="accordion">' . $atts['titel'] . '</h3><div class="panel">' . $content . '</div></div>';

  }

  add_shortcode( 'fold', 'fold_shortcode' );

/* Don't delete this closing tag. */
?>
